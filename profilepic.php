<?php
/************************************
**          BSD Licensed           **
**   Developed by Timothy Rogers   **
*************************************/

// Connect to the OSProfile Server or Robust for newer profiles
$mysqli = new mysqli("localhost", "user", "pass", "osprofiles");
if ($mysqli->connect_errno) {
    die("Connect failed: %s\n". $mysqli->connect_error);
}

// Get the userid from GET
$uuid = $_GET['userid'];

// Now sending request to find the data of the userid submitted
$result = $mysqli->query("SELECT * FROM userprofile WHERE useruuid='$uuid'");  

// Fetch our results
$row = $result->fetch_array(MYSQLI_BOTH);
// Now we print out the UUID of the asset which is the profile image
echo $row['profileImage'];
$result->close();
$mysqli->close();
?>