OpenSim-Profile-Pic-Fetcher
=====================

Used for fetching the UUID of the image on someone's profile

Setup
=====================
1. Place on server
2. Fill in the database information
3. Point your scripts at it

How To Use
=====================
http://example.com/profilepic.php?userid=(uuid goes here)