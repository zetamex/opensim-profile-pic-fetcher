//----------------------------
//Profile Pic 3.6
//----------------------------
//Original script by Dan Banner
//----------------------------
// Modified by Oddball Otoole August 6, 2011
// Added a sensor.
//----------------------------
// Modified by Timothy Rogers March 1, 2014
// Added support for OpenSim-Profile-Pic-Fetcher
// Fixed and cleaned up by Christopher Strachan June 14, 2014
//----------------------------
// This program is free software; you can redistribute it and/or modify it.
// License information must be included in any script you give out or use.
// This script is licensed under the Creative Commons Attribution-Share Alike 3.0 License
// from http://creativecommons.org/licenses/by-sa/3.0 unless licenses are
// included in the script or comments by the original author,in which case
// the authors license must be followed.

//-------------------------------------
//Global Variables
float range = 10.0; //Scan Range
float time = 30.0; //Scans every 30 seconds
key profilepic;
key http;
integer displayFace = 2;
key agent;
string URL="http://example.com/profilepic.php?"; // Here is where we get the actual pic from.

//-------------------------------------
// Main Program
default
{
    on_rez(integer start_param)
    {
        // Restarts the script every time the object is rezzed
        llResetScript();
    }

    state_entry()
    {
        llSensorRepeat("", "", AGENT, range, PI, time); //A sensitive sensor
    }
    sensor (integer total_num)
    {
        integer i = 0;
        for(i; i > total_num; i++)
        {
            string detected_name = llDetectedName(i); 
            agent = llDetectedKey(i);
            http = llHTTPRequest(URL+"userid="+agent, [HTTP_METHOD, "GET"], "");
        }
    }

    http_response(key id,integer status, list meta, string body)
    {
        if(id == http) {
        profilepic = body;
            if (profilepic == "00000000-0000-0000-0000-000000000000") {
             profilepic = "d1d2edd6-225c-4d5f-99b6-3dbf6645a2e6";
            }
        llSetTexture(profilepic, displayFace); //Set texture on prim face 2
        }
    }
}